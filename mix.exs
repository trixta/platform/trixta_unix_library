defmodule TrixtaUnixLibrary.MixProject do
  use Mix.Project

  def project do
    [
      app: :trixta_unix_library,
      version: "0.1.1",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      aliases: aliases(),
      name: "TrixtaUnixLibrary",
      docs: [main: "TrixtaUnixLibrary", extras: ["README.md"]],
      dialyzer: [plt_add_deps: :transitive],
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ]
      # if you want to use espec,
      # test_coverage: [tool: ExCoveralls, test_task: "espec"]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {TrixtaUnixLibrary.Application, []},
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:ethereumex, "~> 0.5.5"},
      # {:blockchain, "~> 0.1.6"},
      # {:etherscan, git: "https://github.com/devlin-playlogix/etherscan.git"},
      # {:etherscan, "~> 2.0.2"},
      {:poison, "~> 4.0"},
      {:httpotion, "~> 3.1.0"},
      {:credo, "~> 1.5.6", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.1.0", only: [:dev, :test], runtime: false},
      {:excoveralls, "~> 0.12", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.21", only: [:dev, :test], runtime: false},
      {:inch_ex, "~> 2.0", only: [:dev, :test], runtime: false},
      # {:ex_abi, "~> 0.2.0"},
      {:trixta_utils, path: "../trixta_utils"},
      {:brod, "~> 3.15"}
    ]
  end

  def aliases do
    [
      review: ["coveralls", "dialyzer", "inch", "hex.audit", "hex.outdated", "credo --strict"]
    ]
  end

  defp description() do
    "The Trixta Library Application."
  end

  defp package() do
    [
      licenses: ["AGPL-3.0-or-later"],
      links: %{"GitLab" => "https://gitlab.com/trixta/platform/trixta_unix_library"}
    ]
  end
end
