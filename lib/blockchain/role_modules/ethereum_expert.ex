# defmodule TrixtaLibrary.BlockChain.EthereumExpert do
#   @moduledoc """
#   Functions for working with the Ethereum blockchain
#   """
#   alias TrixtaLibrary.BlockChain.HexHelpers

#   @doc """
#   Calls the given function on the given smart contract.
#   This does not create a transaction, but only does a GET call.

#   ## Parameters
#     - function_signature : Function signature as a string, including the data types of each argument. It should be of the form functionName(string,bool,etc)
#     - params : List of values for each of the contract function's arguments. Can be of various datatypes.
#     - opts.ethereum_ex_url : Endpoint of the Ethereum blobkchain node.
#     - opts.contract_address : Address of the deployed smart contract.

#     Returns {:ok, result} if successful.

#     NOTE: If the result is from an enum in the contract, it won't be decoded here.
#     It's up to the caller to map this return value to the corresponding enum value in the contract.
#   """
#   def eth_call(function_signature, params, opts)
#       when is_binary(function_signature) and is_list(params) do
#     case Ethereumex.HttpClient.eth_call(
#            %{
#              data: "0x" <> (ABI.encode(function_signature, params) |> Base.encode16()),
#              to: Map.get(opts, "contract_address")
#            },
#            "latest",
#            url: Map.get(opts, "ethereum_ex_url")
#          ) do
#       {:ok, value} ->
#         {"success", %{"value" => value}}

#       err ->
#         {"error", %{"details" => err}}
#     end
#   end

#   @doc """
#   Creates and sends a transaction to the blockchain
#   to call a particular function.

#   ## Parameters
#     - function_signature : Function signature as a string, including the data types of each argument. It should be of the form functionName(string,bool,etc)
#     - params : List of values for each of the contract function's arguments. Can be of various datatypes.
#     - opts.private_key : Private key used to sign the transaction. Must be a hex string, with or without the leading "0x". Case doesn't matter.
#     - opts.ethereum_ex_url : Endpoint of the Ethereum blobkchain node.
#     - opts.contract_address : Address of the deployed smart contract.
#   """
#   def eth_send_raw_transaction(function_signature, params, opts)
#       when is_binary(function_signature) and is_list(params) do
#     encoded_params =
#       data_types_from_function_signature(function_signature)
#       |> encode_addresses_for_transaction(params)

#     private_key = Map.get(opts, "private_key") |> ensure_private_key_decoded()

#     if not is_binary(private_key) do
#       raise "Trying to send an eth transaction from invalid private key #{inspect(private_key)} . Please make sure the private_key is passed in correctly through the opts."
#     end

#     {:ok, public_key} = Blockchain.Transaction.Signature.get_public_key(private_key)

#     public_key = binary_part(public_key, 1, byte_size(public_key) - 1)

#     from_address =
#       public_key
#       |> Blockchain.Transaction.Signature.address_from_public()
#       |> Base.encode16()

#     next_nonce = get_pending_nonce(from_address, opts)

#     transaction_data =
#       %Blockchain.Transaction{
#         data: ABI.encode(function_signature, encoded_params),
#         # TODO: These should be tweaked depending on the contract / network.
#         gas_limit: 3_000_000,
#         gas_price: 16_000_000_000,
#         init: <<>>,
#         nonce: next_nonce,
#         to:
#           Map.get(opts, "contract_address")
#           |> String.slice(2..-1)
#           |> Base.decode16(case: :mixed)
#           |> elem(1)
#       }
#       |> Blockchain.Transaction.Signature.sign_transaction(private_key)

#     transaction_data =
#       transaction_data
#       |> Blockchain.Transaction.serialize()
#       |> ExRLP.encode()
#       |> Base.encode16(case: :lower)

#     case Ethereumex.HttpClient.eth_send_raw_transaction("0x" <> transaction_data,
#            url: Map.get(opts, "ethereum_ex_url")
#          ) do
#       {:ok, tx_hash} when is_binary(tx_hash) ->
#         if String.valid?(tx_hash) and String.length(tx_hash) == 66 do
#           # The transaction has been submitted to the blockchain and its hash returned.
#           {"submitted", %{"tx_hash" => tx_hash}}
#         else
#           {"error", %{"details" => "Valid tx hash was not returned when submitting transaction."}}
#         end

#       {:error, details} when is_map(details) ->
#         {"error", details}

#       err ->
#         {"error",
#          %{
#            "details" =>
#              "Unknown error while submitting transaction to blockchain: #{inspect(err)}"
#          }}
#     end
#   end

#   @doc """
#   Checks whether the given transaction was successfully mined, mining failed or not mined yet.

#   ## Parameters
#     - tx_hash :
#   """
#   def is_transaction_mined(tx_hash, ethereum_ex_url) do
#     case Ethereumex.HttpClient.eth_get_transaction_receipt([tx_hash], url: ethereum_ex_url) do
#       {:ok, %{"status" => status} = receipt} ->
#         {tag_from_receipt_status(status), receipt}

#       _ ->
#         {"not_mined_yet", %{}}
#     end
#   end

#   # Returns the list of types, e.g. ["string", "address"] of each datatype in a function signature.
#   # The signature is in the format "my_function(string,address)"
#   defp data_types_from_function_signature(function_signature) do
#     Regex.run(~r/\((.*)\)/, function_signature) |> Enum.at(1) |> String.split(",")
#   end

#   # Encodes all values of type 'address', because Ethereumex requires them to be in a specific format, i.e. passed through :binary.decode_unsigned/1
#   defp encode_addresses_for_transaction(param_types, param_values) do
#     Enum.zip(param_types, param_values)
#     |> Enum.map(fn {type, value} ->
#       if type == "address" and is_binary(value) and String.valid?(value) and
#            String.length(value) == 42 and String.starts_with?(value, "0x") do
#         # This is most likely an Ethereum address
#         value
#         |> HexHelpers.hex_address_to_bytes()
#         |> :binary.decode_unsigned()
#       else
#         # Not an address type. Don't encode.
#         value
#       end
#     end)
#   end

#   # A 0x1 status field means mining success.
#   # 0x0 means mining failure.
#   defp tag_from_receipt_status(status) when is_binary(status) do
#     case Integer.parse(status |> String.replace("0x", "") |> String.downcase(), 16) do
#       {1, _} ->
#         "mining_succeeded"

#       {0, _} ->
#         "mining_failed"

#       _ ->
#         "not_mined_yet"
#     end
#   end

#   defp tag_from_receipt_status(_) do
#     "not_mined_yet"
#   end

#   defp get_pending_nonce(address, opts \\ %{}) do
#     case Ethereumex.HttpClient.eth_get_transaction_count(
#            "0x" <> address,
#            "pending",
#            url: Map.get(opts, "ethereum_ex_url")
#          ) do
#       {:ok, nonce} ->
#         nonce
#         |> HexHelpers.hex_to_decimal()

#       error ->
#         raise "Error getting pending nonce: #{inspect(error)}"
#     end
#   end

#   defp ensure_private_key_decoded(private_key) do
#     if String.valid?(private_key) do
#       private_key
#       |> String.replace("0x", "")
#       |> String.downcase()
#       |> Base.decode16!(case: :lower)
#     else
#       # Assume this is already encoded.
#       private_key
#     end
#   end
# end
