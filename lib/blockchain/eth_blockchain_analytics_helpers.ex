# defmodule TrixtaLibrary.BlockChain.ETHCryptoAnalyticsHelpers do
#   @moduledoc """
#   Helper functions to access flow execution logs from the rebel engine.
#   """
#   require Logger
#   alias TrixtaLibrary.BlockChain.HexHelpers

#   def get_eth_price(network \\ :default) do
#     Etherscan.get_eth_price(network)
#   end

#   def get_eth_supply(network \\ :default) do
#     Etherscan.get_eth_supply(network)
#   end

#   def eth_gas_price(network \\ :default) do
#     Etherscan.eth_gas_price(network)
#   end

#   def get_contract_details(address, network \\ :default) do
#     task_tuples = []

#     task_tuples = [
#       {"eth_price", Task.async(fn -> Etherscan.get_eth_price(network) end)} | task_tuples
#     ]

#     task_tuples = [
#       {"balance", Task.async(fn -> Etherscan.get_balance(address, network) end)} | task_tuples
#     ]

#     task_tuples = [
#       {"contract_details", Task.async(fn -> Etherscan.get_contract_source(address, network) end)}
#       | task_tuples
#     ]

#     task_tuples = [
#       {"normal_transactions",
#        Task.async(fn ->
#          TrixtaMetrics.get_last_25_transactions(address)
#        end)}
#       | task_tuples
#     ]

#     task_tuples = [
#       {"internal_transactions",
#        Task.async(fn ->
#          # Note: The offset should not be removed. There have been observed inconsistencies in the default page size returned
#          # by Etherscan
#          Etherscan.get_internal_transactions(
#            address,
#            %{page: 1, offset: 25, sort: "desc"},
#            network
#          )
#        end)}
#       | task_tuples
#     ]

#     task_tuples = [
#       {
#         "token_transactions",
#         Task.async(fn ->
#           TrixtaMetrics.get_last_25_erc20_token_transactions(address)
#         end)
#       }
#       | task_tuples
#     ]

#     # Unzip task tuples into separate lists
#     {keys, tasks} =
#       task_tuples
#       |> Enum.unzip()

#     # Helper function to extract results or kill time out tasks
#     get_task_results = fn {task, task_result} ->
#       # Shutdown the task if it timed out
#       case task_result || Task.shutdown(task, :brutal_kill) do
#         # This pattern match is the only bti of code that is specific to the etherscan api. The rest can be easily abstracted to a parallel execution step
#         {:ok, {:ok, results}} -> results
#         # This pattern is for elastic search calls, same notes as above comment
#         {:ok, results} -> results
#         _ -> nil
#       end
#     end

#     task_results =
#       tasks
#       |> Task.yield_many(15000)
#       |> Enum.map(get_task_results)

#     # Rezip keys with task results and contever to map
#     result =
#       keys
#       |> Enum.zip(task_results)
#       |> Map.new()

#     {eth_price_usd, _} = Map.get(result, "eth_price") |> Map.get("ethusd") |> Float.parse()
#     {eth_balance, _} = Map.get(result, "balance") |> Float.parse()

#     # Add eth_value to results
#     result =
#       if eth_price_usd && eth_balance do
#         Map.put(result, "eth_value_usd", Float.to_string(eth_price_usd * eth_balance))
#       else
#         Map.put(result, "eth_value_usd", "0.0")
#       end

#     {:ok, result}
#   end

#   def get_contract_abi_from_address(address, network \\ :default) do
#     try do
#       Etherscan.get_contract_abi(address, network)
#     rescue
#       e -> {:error, e}
#     end
#   end

#   def get_contract_source_from_address(address, network \\ :default) do
#     Etherscan.get_contract_source(address, network)
#   end

#   def get_normal_transactions_from_address(address, network \\ :default) do
#     Etherscan.get_transactions(address, network)
#   end

#   def get_internal_transactions_from_address(address, network \\ :default) do
#     Etherscan.get_internal_transactions(address, network)
#   end

#   def get_logs(params, network \\ :default) do
#     try do
#       Etherscan.get_logs(params, network)
#     rescue
#       ex ->
#         {:error, Exception.message(ex)}
#     end
#   end

#   def get_latest_block_offset(
#         offset,
#         network \\ :default
#       ) do
#     {:ok, latest_block} = Etherscan.eth_block_number(network)
#     {"success", %{"result" => latest_block + offset}}
#   end

#   def get_parsed_logs(%{
#         "address" => address,
#         "network" => network,
#         "type" => _type,
#         "from" => from_block,
#         "to" => to_block,
#         "storage_key" => storage_key
#       }) do
#     # rather than rely on Etherscan to give us the latest, let's make it explicit so we can track our progress better
#     {:ok, latest_block} = Etherscan.eth_block_number(network)

#     # check that sufficient blocks have been made to justify an API request
#     if to_block == "latest" and from_block + 5 > latest_block do
#       {"deferred", %{}}
#     else
#       to_block =
#         if to_block == "latest" do
#           latest_block
#         else
#           to_block
#         end

#       params = %{
#         # Ethereum blockchain address
#         address: address,
#         # Start block number
#         fromBlock: from_block,
#         # End block number
#         toBlock: to_block,
#         # The first topic filter`
#         topic0: "",
#         # The topic operator between topic0 and topic1
#         topic0_1_opr: "and",
#         # The second topic filter
#         topic1: "",
#         # The topic operator between topic1 and topic2
#         topic1_2_opr: "and",
#         # The third topic filter
#         topic2: "",
#         # The topic operator between topic2 and topic3
#         topic2_3_opr: "and",
#         topic3: ""
#         # The fourth topic filter
#       }

#       logs = get_logs(params, network)

#       case logs do
#         {:error, message} ->
#           {"error", %{"message" => message}}

#         {:ok, "Error!"} ->
#           {"error", %{"message" => "Error!"}}

#         {:ok, nil} ->
#           {"error", %{"message" => "result is nil"}}

#         {:ok, result} ->
#           {result, next_from_block_number} =
#             case length(result) do
#               1000 ->
#                 # Because etherscan limits results to 1000 records,
#                 # we need to assume that we might not have gotten all the records from the last block in the result
#                 # Therefore we remove those items and set things up to fetch them starting at that point next time.
#                 last_block_number =
#                   List.last(result).blockNumber
#                   |> HexHelpers.hex_to_decimal()

#                 new_result =
#                   Enum.reject(result, fn log -> log.blockNumber == last_block_number end)

#                 {new_result, last_block_number}

#               0 ->
#                 # No results found, update the next block to the current latest block number.
#                 {result, to_block}

#               _ ->
#                 last_block_number =
#                   List.last(result).blockNumber
#                   |> HexHelpers.hex_to_decimal()

#                 {result, last_block_number + 1}
#             end

#           {
#             "success",
#             %{
#               "address" => address,
#               "next_from_block_number" => next_from_block_number,
#               "storage_key" => storage_key,
#               "result" =>
#                 result
#                 |> Enum.map(fn item ->
#                   item
#                   |> Map.put(
#                     :gasPrice,
#                     item.gasPrice
#                     |> HexHelpers.hex_to_decimal()
#                   )
#                   |> Map.put(
#                     :blockNumber,
#                     item.blockNumber
#                     |> HexHelpers.hex_to_decimal()
#                   )
#                   |> Map.put(
#                     :gasUsed,
#                     item.gasUsed
#                     |> HexHelpers.hex_to_decimal()
#                   )
#                   |> Map.put(
#                     :timeStamp,
#                     item.timeStamp
#                     |> HexHelpers.hex_to_decimal()
#                   )
#                 end)
#             }
#           }

#         _ ->
#           {"error", %{"message" => "unknown"}}
#       end
#     end
#   end

#   def get_parsed_logs(%{
#         "address" => address,
#         "type" => type,
#         "from" => from_block,
#         "to" => to_block,
#         "storage_key" => storage_key
#       }) do
#     get_parsed_logs(%{
#       "address" => address,
#       "network" => "default",
#       "type" => type,
#       "from" => from_block,
#       "to" => to_block,
#       "storage_key" => storage_key
#     })
#   end

#   def decode_contract_event(address, raw_log) when is_binary(address) do
#     case get_contract_abi_from_address(address) do
#       {:ok, abi} -> decode_contract_event(abi, raw_log)
#       _ -> {:error, "Could not fetch ABI for contract"}
#     end
#   end

#   # Tries to parse a contract event from the given raw log and abi.
#   def decode_contract_event(abi, raw_log) do
#     # The ABI library needs 4 topics, even if some of them are nil.
#     topics =
#       if length(raw_log.topics) < 4 do
#         Enum.reduce(length(raw_log.topics)..3, raw_log.topics, fn _i, acc ->
#           acc ++ [nil]
#         end)
#       else
#         raw_log.topics
#       end
#       |> Enum.map(fn topic ->
#         decode_hex(topic)
#       end)

#     decode_args =
#       [ABI.parse_specification(abi, include_events?: true)] ++
#         topics ++ [decode_hex(raw_log.data)]

#     case apply(ABI.Event, :find_and_decode, decode_args) do
#       {%ABI.FunctionSelector{function: evt_name}, inputs} ->
#         {:ok, evt_name, decode_inputs(inputs)}

#       _ ->
#         Logger.warn(
#           "Could not decode Ethereum event. ExABI returned an error. Contract address: #{
#             inspect(raw_log.address)
#           } . Log topics: #{inspect(raw_log.topics)} . Log data field: #{inspect(raw_log.data)}"
#         )

#         {:error, "Could not decode Ethereum events from raw hex data"}
#     end
#   end

#   defp decode_inputs(inputs_from_abi_lib) when is_list(inputs_from_abi_lib) do
#     inputs_from_abi_lib
#     |> Enum.reduce(%{}, fn item, acc ->
#       acc |> Map.merge(parse_decoded_event_input(item))
#     end)
#   end

#   defp decode_inputs(_) do
#     # Assume no inputs could be decoded.
#     %{}
#   end

#   defp decode_hex(hex) when is_binary(hex) do
#     hex |> String.downcase() |> String.replace_prefix("0x", "") |> Base.decode16!(case: :lower)
#   end

#   defp decode_hex(_) do
#     nil
#   end

#   defp parse_decoded_event_input({arg_name, "address", _indexed, addr}) do
#     # Ethereum addresses are returned from ExABI as binaries.
#     # We encode them to a string.
#     %{arg_name => ("0x" <> Base.encode16(addr)) |> String.downcase()}
#   end

#   defp parse_decoded_event_input({arg_name, _type, _indexed, val}) do
#     # Other values include 'bool'.
#     %{arg_name => val}
#   end

#   def get_parsed_transactions(%{
#         "address" => address,
#         "network" => network,
#         "type" => _type,
#         "from" => from_block,
#         "to" => to_block,
#         "storage_key" => storage_key
#       }) do
#     # rather than rely on Etherscan to give us the latest, let's make it explicit so wße can track our progress better
#     {:ok, latest_block} = Etherscan.eth_block_number(network)

#     # check that sufficient blocks have been made to justify an API request
#     if to_block == "latest" and from_block + 5 > latest_block do
#       {"deferred", %{}}
#     else
#       to_block =
#         if to_block == "latest" do
#           latest_block
#         else
#           to_block
#         end

#       # Note: The offset should not be removed. There have been observed inconsistencies in the default page size returned
#       # by Etherscan
#       params = %{
#         # Start block number
#         startblock: from_block,
#         # End block number
#         endblock: to_block,
#         offset: 1000
#       }

#       txs =
#         try do
#           Etherscan.get_transactions(address, params, network)
#         rescue
#           ex ->
#             {:error, Exception.message(ex)}
#         end

#       case txs do
#         {:error, message} ->
#           {"error", %{"message" => message}}

#         {:ok, "Error!"} ->
#           {"error", %{"message" => "Error!"}}

#         {:ok, nil} ->
#           {"error", %{"message" => "result is nil"}}

#         {:ok, result} ->
#           {result, next_from_block_number} =
#             case length(result) do
#               1000 ->
#                 # Because etherscan limits results to 1000 records,
#                 # we need to assume that we might not have gotten all the records from the last block in the result
#                 # Therefore we remove those items and set thigns up to fetch them starting at that point next time.
#                 last_block_number = List.last(result).blockNumber |> String.to_integer()

#                 new_result =
#                   Enum.reject(result, fn log -> log.blockNumber == last_block_number end)

#                 {new_result, last_block_number}

#               0 ->
#                 # No results found, update the next block to the current latest block number.
#                 {result, to_block}

#               _ ->
#                 last_block_number = List.last(result).blockNumber |> String.to_integer()
#                 {result, last_block_number + 1}
#             end

#           {
#             "success",
#             %{
#               "address" => address,
#               "next_from_block_number" => next_from_block_number,
#               "storage_key" => storage_key,
#               "result" => parse_numbers_in_tx_log(result)
#             }
#           }

#         _ ->
#           {"error", %{"message" => "unknown"}}
#       end
#     end
#   end

#   def get_parsed_transactions(%{
#         "address" => address,
#         "type" => type,
#         "from" => from_block,
#         "to" => to_block,
#         "storage_key" => storage_key
#       }) do
#     get_parsed_transactions(%{
#       "address" => address,
#       "network" => "default",
#       "type" => type,
#       "from" => from_block,
#       "to" => to_block,
#       "storage_key" => storage_key
#     })
#   end

#   def get_parsed_erc20_token_events(%{
#         "address" => address,
#         "network" => network,
#         "type" => type,
#         "from" => from_block,
#         "to" => to_block,
#         "storage_key" => storage_key
#       }) do
#     # rather than rely on Etherscan to give us the latest, let's make it explicit so wße can track our progress better
#     {:ok, latest_block} = Etherscan.eth_block_number(network)

#     # check that sufficient blocks have been made to justify an API request
#     if to_block == "latest" and from_block + 5 > latest_block do
#       {"deferred", %{}}
#     else
#       to_block =
#         if to_block == "latest" do
#           latest_block
#         else
#           to_block
#         end

#       # Note: The offset should not be removed. There have been observed inconsistencies in the default page size returned
#       # by Etherscan
#       params = %{
#         # Start block number
#         startblock: from_block,
#         # End block number
#         endblock: to_block,
#         offset: 1000
#       }

#       txs =
#         try do
#           Etherscan.get_ERC20_transactions(address, params, network)
#         rescue
#           ex ->
#             {:error, Exception.message(ex)}
#         end

#       case txs do
#         {:error, message} ->
#           {"error", %{"message" => message}}

#         {:ok, "Error!"} ->
#           {"error", %{"message" => "Error!"}}

#         {:ok, nil} ->
#           {"error", %{"message" => "result is nil"}}

#         {:ok, result} ->
#           {result, next_from_block_number} =
#             case length(result) do
#               1000 ->
#                 # Because etherscan limits results to 1000 records,
#                 # we need to assume that we might not have gotten all the records from the last block in the result
#                 # Therefore we remove those items and set thigns up to fetch them starting at that point next time.
#                 last_block_number = List.last(result).blockNumber |> String.to_integer()

#                 new_result =
#                   Enum.reject(result, fn log -> log.blockNumber == last_block_number end)

#                 {new_result, last_block_number}

#               0 ->
#                 # No results found, update the next block to the current latest block number.
#                 {result, to_block}

#               _ ->
#                 last_block_number = List.last(result).blockNumber |> String.to_integer()
#                 {result, last_block_number + 1}
#             end

#           {
#             "success",
#             %{
#               "address" => address,
#               "next_from_block_number" => next_from_block_number,
#               "storage_key" => storage_key,
#               "result" => parse_numbers_in_tx_log(result)
#             }
#           }

#         _ ->
#           {"error", %{"message" => "unknown"}}
#       end
#     end
#   end

#   def get_parsed_erc20_token_events(%{
#         "address" => address,
#         "type" => type,
#         "from" => from_block,
#         "to" => to_block,
#         "storage_key" => storage_key
#       }) do
#     get_parsed_erc20_token_events(%{
#       "address" => address,
#       "network" => "default",
#       "type" => type,
#       "from" => from_block,
#       "to" => to_block,
#       "storage_key" => storage_key
#     })
#   end

#   def parse_numbers_in_tx_log(logs) when is_list(logs) do
#     logs |> Enum.map(&parse_numbers_in_tx_log/1)
#   end

#   def parse_numbers_in_tx_log(%Etherscan.Transaction{} = log) do
#     [
#       :blockNumber,
#       :confirmations,
#       :cumulativeGasUsed,
#       :gas,
#       :gasPrice,
#       :gasUsed,
#       :nonce,
#       :timeStamp,
#       :transactionIndex,
#       :value
#     ]
#     |> Enum.reduce(log, fn field_name, acc ->
#       acc |> Map.put(field_name, Map.get(acc, field_name) |> String.to_integer())
#     end)
#   end

#   def get_token_list(query_term) do
#     url = "http://etherscan.io/searchHandler?t=t&term=#{query_term}"

#     response =
#       HTTPotion.get(
#         url,
#         headers: [
#           "Content-Type": "application/json"
#         ],
#         timeout: 10000
#       )

#     case response do
#       %HTTPotion.ErrorResponse{message: message} ->
#         {:error, message}

#       %HTTPotion.Response{status_code: status_code, body: response_body} ->
#         case status_code do
#           200 -> {:ok, response_body}
#           404 -> {:not_found, {response_body}}
#           _any -> {:error, response_body}
#         end
#     end
#   end

#   @doc """
#     Parse the token list into ([[]]) array of arrays, each holding a set of string containing the parts of the regex
#     This pattern is as follows: [
#       [whole_string, token_name, ending token, token_short_hand, token_address]
#     ]
#     Of these it is recommended to use token_name, token_short_hand, token_address
#   """
#   def parse_token_list(query_term) do
#     case get_token_list(query_term) do
#       {:ok, response_body} ->
#         Regex.scan(~r{(([a-zA-Z \.\d])+)\ \(([A-Za-z\d]+)\)\\t([A-Za-z\d]+)\\}, response_body)

#       {not_ok, _response_body} ->
#         {not_ok, message: "Error getting token list"}
#     end
#   end

#   def log_contract_data(%{"result" => result} = _contract_data) when length(result) > 0 do
#     contract_abi =
#       case result |> List.first() |> Map.get(:address) |> get_contract_abi_from_address() do
#         {:ok, abi} -> abi
#         _ -> nil
#       end

#     result
#     |> Enum.each(fn %Etherscan.Log{} = item ->
#       # Our Console Logger formatter will accept the json,
#       # decode it and merge the values with Logger's default values.
#       build_logger_data(item, contract_abi)
#       |> Poison.encode!()
#       |> Logger.info(override_timestamp: item.timeStamp * 1000)

#       :timer.sleep(90)
#     end)

#     {"success", nil}
#   end

#   def log_contract_data(_contract_data) do
#     Logger.info("No contract data to log at this time.")
#     {"success", nil}
#   end

#   @doc """
#   Returns a list of events that could be raised by the given contract.
#   Only available if the contract's ABI is hosted on Etherscan.
#   """
#   def get_contract_event_types(_flow_source_data, input_data) do
#     case get_contract_abi_from_address(input_data["contract_address"], input_data["network"]) do
#       {:ok, abi} ->
#         result =
#           ABI.parse_specification(abi, include_events?: true)
#           # Filter out only the Event entries (exlude Functions):
#           |> Enum.filter(fn selector -> selector.type === :event end)
#           # Create a map with only the info needed by the front-end:
#           |> Enum.map(fn event_selector ->
#             %{
#               "event_name" => event_selector.function,
#               "event_arguments" => event_inputs_from_selector(event_selector)
#             }
#           end)

#         # For use as part of a flow
#         {"success", %{"events" => result}}

#       # No event types could be found, or ABI not available
#       _err ->
#         {"success", %{"events" => []}}
#     end
#   end

#   def log_transaction_data(%{"result" => result} = _transaction_data)
#       when length(result) > 0 do
#     contract_abi =
#       case result |> List.first() |> Map.get(:address) |> get_contract_abi_from_address() do
#         {:ok, abi} -> abi
#         _ -> nil
#       end

#     result
#     |> Enum.each(fn %Etherscan.Transaction{} = item ->
#       # This will be formatted by the Console Logger backend (configured in trixta_space),
#       # resulting in valid JSON that FluentBit can parse.
#       build_logger_data(item, contract_abi)
#       |> Poison.encode!()
#       |> Logger.info(override_timestamp: item.timeStamp * 1000)

#       :timer.sleep(90)
#     end)

#     {"success", nil}
#   end

#   def log_erc20_token_event_data(%{"result" => result} = _transaction_data)
#       when length(result) > 0 do
#     contract_abi =
#       case result |> List.first() |> Map.get(:address) |> get_contract_abi_from_address() do
#         {:ok, abi} -> abi
#         _ -> nil
#       end

#     result
#     |> Enum.each(fn %Etherscan.Transaction{} = item ->
#       # This will be formatted by the Console Logger backend (configured in trixta_space),
#       # resulting in valid JSON that FluentBit can parse.
#       build_logger_data(item, contract_abi, "eth_erc20_token_event")
#       |> Poison.encode!()
#       |> Logger.info(override_timestamp: item.timeStamp * 1000)

#       :timer.sleep(90)
#     end)

#     {"success", nil}
#   end

#   def log_transaction_data(_transaction_data) do
#     Logger.info("No transaction data to log at this time.")
#     {"success", nil}
#   end

#   # Formats the list of inputs in an ABI FunctionSelector as a single list of names and types.
#   defp event_inputs_from_selector(%ABI.FunctionSelector{
#          :type => :event,
#          :input_names => names,
#          :types => types
#        })
#        when length(names) === length(types) do
#     # Some types are expressed as tuples, e.g. {:uint, 256} .
#     # We remove the second element because the front-end doesn't really care about it at this point.
#     types =
#       types
#       |> Enum.map(fn type ->
#         serialize_event_input_type(type)
#       end)

#     Enum.zip(names, types)
#     |> Enum.into(%{})
#   end

#   defp event_inputs_from_selector(selector) do
#     Logger.error("Could not format event inputs from ABI FunctionSelector: #{inspect(selector)}")
#     %{}
#   end

#   # Some ethereum event types contain additional info, like number of bits for uint.
#   # This function removes that extra info so that a type can be more easily serialized for the front-end.
#   defp serialize_event_input_type(type) do
#     case type do
#       {elem1, _} when is_atom(elem1) ->
#         elem1

#       atom when is_atom(atom) ->
#         atom

#       unknown_format ->
#         Logger.error("Cannot serialize Ethereum event input type: #{inspect(unknown_format)}")
#         :unknown
#     end
#   end

#   # Given an Ethereum log from Etherscan,
#   # puts together the body for the Logger message for FluentBit to ingest.
#   defp build_logger_data(%Etherscan.Log{} = item, contract_abi) do
#     %{
#       "cat" => "eth_contract_log",
#       "address" => item.address,
#       "blockNumber" => item.blockNumber,
#       "gasPrice" => item.gasPrice,
#       "gasUsed" => item.gasUsed,
#       "topic" => item.topics
#     }
#     |> enrich_logger_data(item, contract_abi)
#   end

#   defp build_logger_data(
#          %Etherscan.Transaction{} = tx,
#          contract_abi,
#          cat_key \\ "eth_transaction_log"
#        ) do
#     tx
#     |> Map.from_struct()
#     |> Map.new(fn {k, v} -> {Atom.to_string(k), v} end)
#     # This field is very large and we don't need it.
#     |> Map.delete("input")
#     # Makes the Logger output in the FluentBit format
#     |> Map.put("cat", cat_key)
#     |> enrich_logger_data(tx, contract_abi)
#   end

#   # Tries to enrich the logger data with any other Ethereum info it can parse from the Etherscan log.
#   # We currently support decoding Ethereum events.
#   defp enrich_logger_data(generic_logger_data, %Etherscan.Log{} = log_item, contract_abi)
#        when is_list(contract_abi) do
#     case decode_contract_event(contract_abi, log_item) do
#       {:ok, event_name, argument_values} ->
#         # We were able to decode an event from this log. Add it to the Logger data:
#         generic_logger_data
#         |> Map.put("eventName", event_name)
#         |> Map.put("eventArgumentValues", argument_values)

#       _ ->
#         # Could not decode an event from this log. Can't add anything to the generic log info.
#         generic_logger_data
#     end
#   end

#   defp enrich_logger_data(generic_logger_data, _log_item, _contract_abi) do
#     # Could not add anything to the generic logger data.
#     generic_logger_data
#   end
# end
