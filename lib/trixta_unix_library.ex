defmodule TrixtaUnixLibrary do
  @moduledoc """
  Documentation for TrixtaLibrary.
  """

  def require_steps do
    Code.ensure_loaded(Trixta.Steps.Brod.ConsumerFlow.V_0_0_1)
    Code.ensure_loaded(Trixta.Steps.Brod.ConsumerFlow.V_1_1_0)
    Code.ensure_loaded(Trixta.Steps.Brod.Producer.V_1_0_0)
  end
end
