defmodule Trixta.Steps.Brod.ConsumerFlow.V_0_0_1 do
  @behaviour Trixta.Steps.Step
  alias TrixtaSpaceWeb.Step.Common
  alias TrixtaSpaceWeb.Step.Helpers
  alias TrixtaSpaceWeb.Step.Input
  @vsn "0.0.1"

  @kafka_hosts_setting "kafka_hosts_setting"
  @topic_setting "topic_setting"
  @flow_to_run "flow_to_run"
  @debug "debug"

  def compile(_env, config) do
    key = config["key"]

    [
      kafka_hosts,
      topic,
      flow_to_run,
      debug
    ] = Helpers.apply_defaults(config["settings"], default_settings())

    main_process_ast =
      quote do
        (fn t ->
           {_label, topic} = Input.V_0_0_1.resolve(unquote(topic), t)

           {:ok, _} = Application.ensure_all_started(:brod)

           :ok =
             Trixta.Steps.Brod.ConsumerFlow.V_0_0_1.BrodSubscriber.bootstrap(
               unquote(kafka_hosts),
               unquote(flow_to_run),
               topic,
               unquote(debug)
             )

           Process.sleep(:infinity)

           {"ok", %{"result" => true}}
         end).(t)
      end

    execute_ast =
      Common.build_execute_ast(
        key,
        nil,
        main_process_ast,
        nil,
        nil,
        config["flow_module"],
        __MODULE__,
        config["step_type_map"]
      )

    quote do
      unquote(execute_ast)
      
      def summarize() do
        unquote(Common.summarize_step(config, __MODULE__, default_settings()))
      end

      def explain(already_explained \\ []) do
        if unquote(key) in already_explained do
          "[is recursive]"
        else
          "TODO"
        end
      end

      def warnings(t \\ nil) do
        [] ++
          Input.V_0_0_1.batch_warnings(
            %{
              "t" => t,
              "key" => unquote(key),
              "step_type" => unquote(inspect(__MODULE__))
            },
            unquote(Helpers.apply_defaults(config["settings"], default_settings()))
          )
      end
    end
  end

  def json_decoded_to_definition("settings", %{
        @kafka_hosts_setting => kafka_hosts_setting,
        @topic_setting => topic_setting,
        @flow_to_run => flow_to_run,
        @debug => debug
      }) do
    [
      Enum.map(kafka_hosts_setting, fn %{"hostname" => hostname, "port" => port} ->
        {hostname, port}
      end),
      convert_json_decoded_to_definition(topic_setting),
      flow_to_run,
      debug
    ]
  end

  defp convert_json_decoded_to_definition(%{"input_type" => input_type} = value) do
    Input.V_0_0_1.json_decoded_to_definition(input_type, value)
  end

  def definition_to_json_encodable("settings", [
        kafka_hosts_setting,
        topic_setting,
        flow_to_run,
        debug
      ]) do
    %{
      @kafka_hosts_setting =>
        Enum.map(kafka_hosts_setting, fn {hostname, port} ->
          %{
            "hostname" => hostname,
            "port" => port
          }
        end),
      @topic_setting =>
        convert_definition_to_json_encodable(topic_setting || [Input.None.V_0_0_1]),
      @flow_to_run => flow_to_run,
      @debug => debug
    }
  end

  defp convert_definition_to_json_encodable([input_type | params] = value)
       when is_list(value) do
    Input.V_0_0_1.definition_to_json_encodable(input_type, params)
  end

  defp convert_definition_to_json_encodable([input_type] = value)
       when is_list(value) do
    Input.V_0_0_1.definition_to_json_encodable(input_type, [])
  end

  def input_json_schema() do
    %{
      "title" => "Consumer Flow",
      "description" => "",
      "type" => "object",
      "properties" => %{
        "settings" => %{
          "definitions" => Input.V_0_0_1.json_schema_definitions(),
          "type" => "object",
          "title" => "",
          "properties" => %{
            @kafka_hosts_setting => %{
              "title" => "Kafka Hosts",
              "type" => "array",
              "default" => [],
              "items" => %{
                "type" => "object",
                "properties" => %{
                  "hostname" => %{
                    "type" => "string"
                  },
                  "port" => %{
                    "type" => "number"
                  }
                }
              }
            },
            @topic_setting => %{
              "title" => "Topic",
              "$ref" => Input.V_0_0_1.input_json_schema_ref()
            },
            @flow_to_run => %{
              "title" => "Flow to run",
              "type" => "string",
              "default" => ""
            },
            @debug => %{
              "title" => "Debug",
              "description" => "",
              "type" => "boolean",
              "default" => false
            }
          }
        },
        "input" => %{
          "type" => "null",
          "title" => "",
          "description" => "There is no input for this step"
        },
        "result_function" => %{
          "type" => "null",
          "title" => "",
          "description" => "There is no result for this step"
        },
        "branch" => Common.branch_json_schema()
      }
    }
  end

  def input_render_schema() do
    %{
      "themes" => %{
        "trixta_preset_3" => %{
          "colors" => %{
            "background" => "#cccccc",
            "executing" => "#36c0f7",
            "warning" => "#FBBD08",
            "error" => "#FF526F",
            "none" => "#f3f4fa"
          }
        },
        "trixta_preset_2" => %{
          "colors" => %{
            "background" => "#cccccc",
            "executing" => "#fec342",
            "warning" => "#FBBD08",
            "error" => "#FF526F",
            "none" => "#f3f4fa"
          }
        },
        "trixta_preset_1" => %{
          "colors" => %{
            "background" => "#cccccc",
            "executing" => "#e449b2",
            "warning" => "#FBBD08",
            "error" => "#FF526F",
            "none" => "#f3f4fa"
          }
        }
      },
      "render_options" => %{
        "header_section" => %{
          "show_connected_actions" => false
        },
        "body_section" => %{
          "show_contents_vertical" => false,
          "show_contents_horizontal" => false
        },
        "footer_section" => %{
          "loop_options" => false
        }
      }
    }
  end

  def input_ui_schema() do
    %{
      "settings" => %{
        @topic_setting => Input.V_0_0_1.ui_settings()
      },
      "input" => Input.V_0_0_1.ui_settings(),
      "branch" => Common.branch_ui_settings()
    }
  end

  # copying from settings and replacing the keys we looking to update
  def meta_data() do
    %{
      Common.meta_data_settings()
      | # step specific updates to meta data
        "description" =>
          "Uses the Elixir Brod library to consume a Kafka stream. Some setup required first.",
        "created" => "01 Nov, 2019",
        "title" => "Kafka Consumer",
        # the grouping of the steps list of strings
        "categories" => %{
          # standard, advanced, beta
          "diff" => ["advanced", "beta"],
          # logic, interact, data, non-functional, code
          "func" => ["data"],
          # response, effect, mixed
          "direction" => [],
          # internal, external, mixed
          "where" => ["external"],
          # this will contain a list of specific risk tags for steps where needed
          "risk" => []
        },
        # link to documentation
        "doc" =>
          "https://docs.trixta.com/glossary/individual-options-per-step#step_brod_consumer_flow"
    }
  end

  def default_settings() do
    [
      nil,
      [Input.None.V_0_0_1],
      nil,
      false
    ]
  end
end

## The brod_group_subscriber implementation.
defmodule Trixta.Steps.Brod.ConsumerFlow.V_0_0_1.BrodSubscriber do
  @behaviour :brod_group_subscriber
  require Logger
  require Record
  import Record, only: [defrecord: 2, extract: 2]
  defrecord :kafka_message, extract(:kafka_message, from_lib: "brod/include/brod.hrl")

  ## API for demo
  def bootstrap(kafka_hosts, flow_to_run, topic, debug) do
    ## A group ID is to be shared between the members (which often run in
    ## different Erlang nodes or even hosts).
    group_id = "brod_group"
    ## Different members may subscribe to identical or different set of topics.
    ## In the assignments, a member receives only the partitions from the
    ## subscribed topic set.
    topic_set = [topic]
    ## In this demo, we spawn two members in the same Erlang node.
    member_clients = [
      :brod_consumer_client
    ]

    :ok =
      bootstrap_subscribers(member_clients, kafka_hosts, group_id, flow_to_run, topic_set, debug)
  end

  ## brod_group_subscriber callback
  def init(_group_id, _callback_init_args = {client_id, flow_to_run, topics, debug}) do
    ## For demo, spawn one message handler per topic-partition.
    ## Depending on the use case:
    ## It might be enough to handle the message locally in the subscriber process
    ## without dispatching to handlers. i.e. retrun {:ok, :ac, callback_state} here.
    ## Or there could be a pool of handlers if the messages can be processed
    ## in arbitrary order.
    handlers = spawn_message_handlers(client_id, flow_to_run, topics, debug)
    {:ok, %{handlers: handlers}}
  end

  ## brod_group_subscriber callback
  def handle_message(topic, partition, message, %{handlers: handlers} = state) do
    pid = handlers["#{topic}-#{partition}"]

    ## send the message to message handler process for async processing
    send(pid, message)

    ## or return {:ok, :ack, state} in case message can be handled synchronously here
    {:ok, state}
  end

  ## API for internal use
  def message_handler_loop(topic, flow_to_run, partition, subscriber_pid, debug) do
    receive do
      msg ->
        %{offset: offset, value: value} = Enum.into(kafka_message(msg), %{})

        if rem(offset, 10) == 0 do
          Logger.info(
            "#{inspect(self())} #{topic}-#{partition} Offset: #{offset}, Value: #{value}"
          )
        end

        ## send the async ack to group subscriber
        ## the offset will be eventually committed to kafka
        :brod_group_subscriber.ack(subscriber_pid, topic, partition, offset)

        converted_value =
          case Poison.decode(value) do
            {:ok, result} ->
              result

            # Could not decode, so value is not json, just return as is
            {:error, error_detail} ->
              value

            {:error, error_detail, _} ->
              value
          end

        TrixtaSpaceWeb.Flow.Helpers.run_trixta_flow(
          flow_to_run,
          %{
            "value" => converted_value
          },
          %{},
          if debug do
            [
              debug: true,
              slowdown: 0,
              inspect: true,
              debug_broadcast: %{
                "role" => "trixta_ide_user"
              }
            ]
          else
            []
          end
        )

        __MODULE__.message_handler_loop(topic, flow_to_run, partition, subscriber_pid, debug)
    after
      1000 ->
        __MODULE__.message_handler_loop(topic, flow_to_run, partition, subscriber_pid, debug)
    end
  end

  defp bootstrap_subscribers([], _kafka_hosts, _group_id, _flow_to_run, _topics, _debug), do: :ok

  defp bootstrap_subscribers(
         [client_id | rest],
         kafka_hosts,
         group_id,
         flow_to_run,
         topics,
         debug
       ) do
    :brod.start_client(kafka_hosts, client_id, _client_config = [{:ssl, true}])

    group_config = [
      offset_commit_policy: :commit_to_kafka_v2,
      offset_commit_interval_seconds: 5,
      rejoin_delay_seconds: 2
    ]

    {:ok, _subscriber} =
      :brod.start_link_group_subscriber(
        client_id,
        group_id,
        topics,
        group_config,
        _consumer_config = [begin_offset: :earliest],
        _callback_module = __MODULE__,
        _callback_init_args = {client_id, flow_to_run, topics, debug}
      )

    bootstrap_subscribers(rest, kafka_hosts, group_id, flow_to_run, topics, debug)
  end

  defp spawn_message_handlers(_client_id, _flow_to_run, [], _debug), do: %{}

  defp spawn_message_handlers(client_id, flow_to_run, [topic | rest], debug) do
    {:ok, partition_count} = :brod.get_partitions_count(client_id, topic)

    handlers =
      Enum.reduce(:lists.seq(0, partition_count - 1), %{}, fn partition, acc ->
        handler_pid =
          spawn_link(__MODULE__, :message_handler_loop, [
            topic,
            flow_to_run,
            partition,
            self(),
            debug
          ])

        Map.put(acc, "#{topic}-#{partition}", handler_pid)
      end)

    Map.merge(handlers, spawn_message_handlers(client_id, flow_to_run, rest, debug))
  end
end
