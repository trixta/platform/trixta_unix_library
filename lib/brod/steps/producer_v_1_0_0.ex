defmodule Trixta.Steps.Brod.Producer.V_1_0_0 do
  @behaviour Trixta.Steps.Step
  alias TrixtaSpaceWeb.Step.Common
  alias TrixtaSpaceWeb.Step.Helpers
  alias TrixtaSpaceWeb.Step.Input
  @vsn "0.0.1"

  @kafka_hosts_setting "kafka_hosts_setting"
  @topic_setting "topic_setting"
  @message_key "message_key"
  @message_value "message_value"
  @options "options"
  @compression "compression"

  def default_settings() do
    [
      [Input.None.V_0_0_1],
      [Input.None.V_0_0_1],
      [Input.None.V_0_0_1],
      [Input.None.V_0_0_1],
      [Input.None.V_0_0_1],
      [Input.None.V_0_0_1]
    ]
  end

  def compile(_env, config) do
    key = config["key"]

    [
      kafka_hosts,
      topic,
      message_key,
      message_value,
      options,
      compression
    ] = Helpers.apply_defaults(config["settings"], default_settings())

    main_process_ast =
      quote do
        (fn t ->
           {_label, topic} = Input.V_0_0_1.resolve(unquote(topic), t)
           {_label, message_value} = Input.V_0_0_1.resolve(unquote(message_value), t)
           {_label, options} = Input.V_0_0_1.resolve(unquote(options), t)
           {_label, kafka_hosts} = Input.V_0_0_1.resolve(unquote(kafka_hosts), t)
           {_label, message_key} = Input.V_0_0_1.resolve(unquote(message_key), t)
           {_label, compression} = Input.V_0_0_1.resolve(unquote(compression), t)

           kafka_hosts_as_tuples =
             String.split(kafka_hosts, ",")
             |> Enum.map(fn host_string ->
               [host, port] = String.split(host_string, ":")
               {host, String.to_integer(port)}
             end)

           options_as_keywords =
             String.split(options, ",")
             |> Enum.map(fn option_value ->
               [option, value] = String.split(option_value, ":")
               # Todo improve this support because creating atoms is a potential mem leak
               # Can't use to_exisitng_atom because query_api_versions
               {String.to_atom(option),
                case option do
                  "ssl" ->
                    if value == "true" do
                      true
                    else
                      false
                    end

                  "query_api_versions" ->
                    if value == "true" do
                      true
                    else
                      false
                    end

                  "sasl" ->
                    [mechanism, username, password] = String.split(value, ";")

                    {case mechanism do
                       "plain" ->
                         :plain

                       "scram_sha_256" ->
                         :scram_sha_256

                       "scram_sha_512" ->
                         :scram_sha_512
                     end, username, password}

                  _ ->
                    value
                end}
             end)

           {:ok, _} = Application.ensure_all_started(:brod)
           producer_client_id = :brod_producer_client

           :brod.start_client(kafka_hosts_as_tuples, producer_client_id, options_as_keywords)

           :brod.start_producer(producer_client_id, topic, [
             {:compression,
              case compression do
                "snappy" ->
                  :snappy

                "gzip" ->
                  :gzip

                "none" ->
                  :no_compression
              end}
           ])

           {:ok, partition_count} = :brod.get_partitions_count(producer_client_id, topic)
           partition = :erlang.phash2(message_key, partition_count)

           message_value =
             case message_value do
               value when is_map(value) or is_list(value) ->
                 case Poison.encode(message_value) do
                   {:ok, encoded} ->
                     encoded

                   {:error, encode_error} ->
                     throw("unable to encode message value")
                 end

               value when is_nil(value) ->
                 throw("nil input is not supported")

               value ->
                 value
             end

           result =
             :brod.produce(producer_client_id, topic, partition, message_key, message_value)

           {"ok", %{"result" => result}}
         end).(t)
      end

    execute_ast =
      Common.build_execute_ast(
        key,
        nil,
        main_process_ast,
        nil,
        nil,
        config["flow_module"],
        __MODULE__,
        config["step_type_map"]
      )

    quote do
      unquote(execute_ast)
      
      def summarize() do
        unquote(Common.summarize_step(config, __MODULE__, default_settings()))
      end

      def explain(already_explained \\ []) do
        if unquote(key) in already_explained do
          "[is recursive]"
        else
          "TODO"
        end
      end

      def warnings(t \\ nil) do
        [] ++
          Input.V_0_0_1.batch_warnings(
            %{
              "t" => t,
              "key" => unquote(key),
              "step_type" => unquote(inspect(__MODULE__))
            },
            unquote(Helpers.apply_defaults(config["settings"], default_settings()))
          )
      end
    end
  end

  def json_decoded_to_definition("settings", value) do
    [
      # Enum.map(value[@kafka_hosts_setting], fn %{"hostname" => hostname, "port" => port} ->
      #   {hostname, port}
      # end),
      convert_json_decoded_to_definition(value[@kafka_hosts_setting] || ""),
      convert_json_decoded_to_definition(value[@topic_setting] || ""),
      convert_json_decoded_to_definition(value[@message_key] || ""),
      convert_json_decoded_to_definition(value[@message_value] || ""),
      # Enum.map(value[@options], fn %{"option" => option, "value" => options_value} ->
      #   [option, convert_json_decoded_to_definition(options_value)]
      # end)
      convert_json_decoded_to_definition(value[@options] || ""),
      convert_json_decoded_to_definition(value[@compression] || "none")
    ]
  end

  defp convert_json_decoded_to_definition(%{"input_type" => input_type} = value) do
    Input.V_0_0_1.json_decoded_to_definition(input_type, value)
  end

  # defp convert_json_decoded_to_definition(list) when is_list(list) do
  #   TrixtaSpace.console("list")
  #   TrixtaSpace.console(list)
  #   Enum.map(list, fn item ->
  #     convert_json_decoded_to_definition(item)
  #   end)
  # end

  def definition_to_json_encodable("settings", [
        kafka_hosts_setting,
        topic_setting,
        message_key,
        message_value,
        options,
        compression
      ]) do
    %{
      @kafka_hosts_setting =>
        convert_definition_to_json_encodable(kafka_hosts_setting || [Input.None.V_0_0_1]),
      # Enum.map(kafka_hosts_setting, fn {hostname, port} ->
      #   %{
      #     "hostname" => hostname,
      #     "port" => port
      #   }
      # end),
      @topic_setting =>
        convert_definition_to_json_encodable(topic_setting || [Input.None.V_0_0_1]),
      @message_key => convert_definition_to_json_encodable(message_key || [Input.None.V_0_0_1]),
      @message_value =>
        convert_definition_to_json_encodable(message_value || [Input.None.V_0_0_1]),
      @options => convert_definition_to_json_encodable(options || [Input.None.V_0_0_1]),
      @compression => convert_definition_to_json_encodable(compression || [Input.None.V_0_0_1])
      # Enum.map(options, fn {option, value} ->

      # TrixtaSpace.console({option, value})
      #   %{
      #     "option" => option,
      #     "value" => value
      #   }
      # end)
    }
  end

  defp convert_definition_to_json_encodable([input_type | params] = value)
       when is_list(value) do
    Input.V_0_0_1.definition_to_json_encodable(input_type, params)
  end

  defp convert_definition_to_json_encodable([input_type] = value)
       when is_list(value) do
    Input.V_0_0_1.definition_to_json_encodable(input_type, [])
  end

  # defp convert_definition_to_json_encodable(list) when is_list(list) do
  #   TrixtaSpace.console("back to encodable")
  #   TrixtaSpace.console(list)
  #   []
  # end

  def input_json_schema() do
    %{
      "title" => "Producer Flow",
      "description" => "",
      "type" => "object",
      "properties" => %{
        "settings" => %{
          "definitions" => Input.V_0_0_1.json_schema_definitions(),
          "type" => "object",
          "title" => "",
          "properties" => %{
            @kafka_hosts_setting => %{
              "title" => "Kafka Hosts",
              "$ref" => Input.V_0_0_1.input_json_schema_ref(),
              "default" => %{"input_type"=> "value", "value"=> %{"string"=> "host:port,host:port,..."}},
              "description" => "The name of the topic",
              "static_schema" => %{
                "type" => "string",
                "data" => %{
                  "default" => "host:port,host:port,..."
                }
                # "type" => "array",
                # "data" => %{
                #   "items" => %{
                #     "type" => "object",
                #     "properties" => %{
                #       "hostname" => %{
                #         "type" => "string",
                #         "title" => "Hostname"
                #       },
                #       "port" => %{
                #         "type" => "number",
                #         "title" => "Port"
                #       }
                #     }
                #   }
                # }
              }
            },
            @topic_setting => %{
              "title" => "Topic",
              "$ref" => Input.V_0_0_1.input_json_schema_ref(),
              "description" => "The name of the topic",
              "default" => %{"input_type"=> "value", "value"=> %{"string"=> "default_topic_name"}},
              "static_schema" => %{
                "type" => "string",
                "data" => %{
                  "default" => "default_topic_name"
                }
              }
            },
            @message_key => %{
              "title" => "Message Key",
              "$ref" => Input.V_0_0_1.input_json_schema_ref(),
              "static_schema" => %{},
              "default" => %{"input_type"=> "value", "value"=> %{"string"=> ""}},
              # "static_schema" => %{
              #   "type" => "string",
              #   "data" => %{
              #     "default" => "default_message_key"
              #   }
              # }
            },
            @message_value => %{
              "title" => "Message Value",
              "$ref" => Input.V_0_0_1.input_json_schema_ref(),
              "default" => %{"input_type"=> "value", "value"=> %{"string"=> ""}},
              "description" => "The contents of the message to send",
              "static_schema" => %{}
            },
            @options => %{
              "title" => "Client Config Options",
              "$ref" => Input.V_0_0_1.input_json_schema_ref(),
              "description" => "The client config option",
              "default" => %{"input_type"=> "value", "value"=> %{"string"=> "ssl:true"}},
              "static_schema" => %{
                "type" => "string",
                "data" => %{
                  "default" => "ssl:true"
                }
                # "type" => "array",
                # "data" => %{
                #   "items" => %{
                #     "type" => "object",
                #     "properties" => %{
                #       "option" => %{
                #         "title" => "Option",
                #         "type" => "string"
                #       },
                #       "value" => %{
                #         "title" => "Value",
                #         "$ref" => Input.V_0_0_1.input_json_schema_ref(),
                #         "description" => "The client config option",
                #         "static_schema" => %{
                #           "type" => "string"
                #         }
                #       }
                #     }
                #   }
                # }
              }
            },
            @compression => %{
              "title" => "Compression",
              "$ref" => Input.V_0_0_1.input_json_schema_ref(),
              "description" => "Compression setting",
              "default" => %{"input_type"=> "value", "value"=> %{"string"=> "none"}},
              "static_schema" => %{
                "type" => "string",
                "data" => %{
                  "default" => "none",
                  "enum" => [
                    "none",
                    "gzip",
                    "snappy"
                  ]
                }
              }
              # "static_schema" => %{
              #   "type" => "boolean",
              #   "data" => %{
              #     "default" => false
              #   }
              # }
            }
          }
        },
        "input" => %{
          "type" => "null",
          "title" => "",
          "description" => "There is no input for this step"
        },
        "result_function" => %{
          "type" => "null",
          "title" => "",
          "description" => "There is no result for this step"
        },
        "branch" => Common.branch_json_schema()
      }
    }
  end

  def input_render_schema() do
    %{
      "themes" => %{
        "trixta_preset_3" => %{
          "colors" => %{
            "background" => "#cccccc",
            "executing" => "#36c0f7",
            "warning" => "#FBBD08",
            "error" => "#FF526F",
            "none" => "#f3f4fa"
          }
        },
        "trixta_preset_2" => %{
          "colors" => %{
            "background" => "#cccccc",
            "executing" => "#fec342",
            "warning" => "#FBBD08",
            "error" => "#FF526F",
            "none" => "#f3f4fa"
          }
        },
        "trixta_preset_1" => %{
          "colors" => %{
            "background" => "#cccccc",
            "executing" => "#e449b2",
            "warning" => "#FBBD08",
            "error" => "#FF526F",
            "none" => "#f3f4fa"
          }
        }
      },
      "render_options" => %{
        "header_section" => %{
          "show_connected_actions" => false
        },
        "body_section" => %{
          "show_contents_vertical" => false,
          "show_contents_horizontal" => false
        },
        "footer_section" => %{
          "loop_options" => false
        }
      }
    }
  end

  def input_ui_schema() do
    %{
      "settings" => %{
        "ui:order" => [
          @kafka_hosts_setting,
          @topic_setting,
          @options,
          @compression,
          @message_key,
          @message_value
        ],
        @topic_setting => Input.V_0_0_1.ui_settings(),
        @message_value => Input.V_0_0_1.ui_settings(),
        @message_key => Input.V_0_0_1.ui_settings(),
        @kafka_hosts_setting => Input.V_0_0_1.ui_settings(),
        @options => Input.V_0_0_1.ui_settings(),
        @compression => Input.V_0_0_1.ui_settings()
      },
      "input" => Input.V_0_0_1.ui_settings(),
      "branch" => Common.branch_ui_settings()
    }
  end

  # copying from settings and replacing the keys we looking to update
  def meta_data() do
    %{
      Common.meta_data_settings()
      | # step specific updates to meta data
        "description" =>
          "Uses the Elixir Brod library to produce to a Kafka stream. Some setup required first.",
        "created" => "01 Nov, 2019",
        "title" => "Kafka Producer",
        # the grouping of the steps list of strings
        "categories" => %{
          # standard, advanced, beta
          "diff" => ["advanced", "beta"],
          # logic, interact, data, non-functional, code
          "func" => ["data"],
          # response, effect, mixed
          "direction" => [],
          # internal, external, mixed
          "where" => ["external"],
          # this will contain a list of specific risk tags for steps where needed
          "risk" => []
        },
        # link to documentation
        "doc" => "https://docs.trixta.com/glossary/individual-options-per-step#step_brod_producer"
    }
  end
end
