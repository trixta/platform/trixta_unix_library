defmodule TrixtaUnixLibrary.Application do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = []

    TrixtaUnixLibrary.require_steps()

    opts = [strategy: :one_for_one, name: TrixtaUnixLibrary.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
