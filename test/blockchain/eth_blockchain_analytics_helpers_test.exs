# Disengaging these tests until a time we need them again. They were not working because we were breaking the rate limit rules without using an API key.
# more can be read here: https://medium.com/etherscan-blog/psa-for-developers-implementation-of-api-key-requirements-starting-from-february-15th-2020-b616870f3746

# defmodule TrixtaLibraryTest.Blockchain.EthBlockchainAnalyticsHelpersTest do
#   @moduledoc """
#   These tests connect to Etherscan to get contract data, so they should be run with an internet connection.
#   """
#   use ExUnit.Case
#   doctest TrixtaLibrary.BlockChain.ETHCryptoAnalyticsHelpers
#   alias TrixtaLibrary.BlockChain.ETHCryptoAnalyticsHelpers, as: Helpers
#   import ExUnit.CaptureLog

#   test "Can query ABI for verified contract" do
#     #  We know this contract has its ABI exposed on Etherscan:
#     {:ok, abi} =
#       Helpers.get_contract_abi_from_address("0x636462621cD419A53cd1dF1725632A4AcF5648B3")

#     # Spot check that this contract has 8 event types:
#     assert abi |> Enum.filter(fn item -> item["type"] === "event" end) |> length === 8
#   end

#   test "Returns error if ABI cannot be loaded" do
#     # We know this is not a valid contract address:
#     assert {:error, _} =
#              Helpers.get_contract_abi_from_address("0xCC01E3DEE425279C30C4a0B8EAF7aC4c07dd434b")
#   end

#   test "Events with non-dynamic parameters get decoded properly" do
#     # We know that this contract (0x174...) has some events logged in block number 7854907.
#     # We test that those events get decoded properly.
#     contract_address = "0x174BfA6600Bf90C885c7c01C7031389ed1461Ab9"

#     log_fetch_params = %{
#       "address" => contract_address,
#       "network" => "default",
#       "type" => "test",
#       "from" => 7_854_907,
#       "to" => 7_854_907,
#       "storage_key" => "test"
#     }

#     {"success", %{"result" => logs}} = Helpers.get_parsed_logs(log_fetch_params)
#     {:ok, _evt_name, _args} = Helpers.decode_contract_event(contract_address, List.first(logs))
#   end

#   # TODO: Skipping this test for now because ex_abi throws an exception trying to decode strings and some indexed arguments.
#   # This should be possible to get working, but most of our analytics contracts in prod have
#   # much simpler events. We can circle back to fixing this later.
#   @tag :skip
#   test "Events with strings get decoded properly" do
#     # This is a simple contract we deployed to mainnet.
#     # It throws an event with the following signature when a function is called on it:
#     # event SomethingHappened (
#     #   string nonIndexedString,
#     #   string indexed indexedString,
#     #   uint nonIndexedInt,
#     #   uint indexed indexedInt,
#     #   bool nonIndexedBool,
#     #   address nonIndexedAddress,
#     #   address indexed indexedAddress
#     # );
#     contract_address = "0xAD4e6095FA1F9bfd05BBed96329F258f20840b40"

#     log_fetch_params = %{
#       "address" => contract_address,
#       "type" => "test",
#       # We created an event on this contract at this specific block.
#       "from" => 7_860_989,
#       "to" => 7_860_989,
#       "storage_key" => "test"
#     }

#     {"success", %{"result" => logs}} = Helpers.get_parsed_logs(log_fetch_params)
#     {:ok, _evt_name, _args} = Helpers.decode_contract_event(contract_address, List.first(logs))
#   end

#   test "Contract events get logged to Logger correctly for FluentBit" do
#     # We know that the contract 0x174B... has a certain log at block number 7_854_907 (chosen completely arbitrarily).
#     # Since it should never change (blockchain is immutable), this test should always pass unless the ABI goes missing.

#     log_fetch_params = %{
#       "address" => "0x174BfA6600Bf90C885c7c01C7031389ed1461Ab9",
#       "network" => "default",
#       "type" => "test",
#       # We know the first log we expect to see on this contract at this block number.
#       "from" => 7_854_907,
#       "to" => 7_854_907,
#       "storage_key" => "test"
#     }

#     {"success", %{"result" => logs}} = Helpers.get_parsed_logs(log_fetch_params)

#     # These should all just be token transfer events, so we only look at the first one.
#     logger_output =
#       capture_log(fn ->
#         Helpers.log_contract_data(%{"result" => [List.first(logs)]})
#       end)

#     # Get all JSON strings from the logger output.
#     # Then check that we have a log line containing both the generic log data and the decoded event name / args
#     json_strings = Regex.scan(~r/\{(?:[^{}]|(?R))*\}/, logger_output) |> List.flatten()

#     assert Enum.any?(json_strings, fn json ->
#              # Decode the logged json and check whether it matches our structured event data:
#              {:ok, logger_data} = Poison.decode(json)
#              has_logged_event_data(logger_data) and has_logged_generic_log_stats(logger_data)
#            end)
#   end

#   test "Transactions are logged properly" do
#     tx_fetch_params = %{
#       startblock: 7_112_384,
#       endblock: 7_112_384,
#       # only fetch 1 transaction...we just want to check that the logging works.
#       offset: 1
#     }

#     {:ok, [tx]} =
#       Etherscan.get_transactions("0x174BfA6600Bf90C885c7c01C7031389ed1461Ab9", tx_fetch_params)

#     # These values were simply copied from Etherscan. We compare them to what's logged by our logger.
#     logger_output =
#       capture_log(fn ->
#         tx = Helpers.parse_numbers_in_tx_log(tx)
#         Helpers.log_transaction_data(%{"result" => [tx]})
#       end)

#     # Get all JSON strings from the logger output.
#     # Then check that we have a log line containing the transaction data
#     json_strings = Regex.scan(~r/\{(?:[^{}]|(?R))*\}/, logger_output) |> List.flatten()

#     assert Enum.any?(json_strings, fn json ->
#              # Decode the logged json and check whether it matches our known transaction values:
#              {:ok, logger_data} = Poison.decode(json)
#              has_logged_transaction_data(logger_data)
#            end)
#   end

#   test "can fetch possible event types + signatures for contract" do
#     # This contract is known to have its ABI on Etherscan:
#     {"success", %{"events" => events}} =
#       Helpers.get_contract_event_types(%{}, %{
#         "contract_address" => "0x636462621cD419A53cd1dF1725632A4AcF5648B3"
#       })

#     assert length(events) === 8
#     # Spot-check two of the events:
#     assert events |> Enum.at(0) === %{
#              "event_arguments" => %{
#                "currency" => :string,
#                "currencyAmount" => :uint,
#                "holder" => :address,
#                "rateToEther" => :uint,
#                "tokenAmount" => :uint,
#                "tx" => :string
#              },
#              "event_name" => "ExternalPurchase"
#            }

#     assert events |> Enum.at(7) === %{
#              "event_arguments" => %{"newOwner" => :address, "previousOwner" => :address},
#              "event_name" => "OwnershipTransferred"
#            }

#     # Check that a contract without ABI doesn't return any event types:
#     # This address was generated randomly.
#     assert Helpers.get_contract_event_types(
#              %{},
#              %{"contract_address" => "0xaa023059688dBfCDF507a85082582e8F38A4C433"}
#            ) === {"success", %{"events" => []}}
#   end

#   # Checks to see whether the logged data matches our known transaction.
#   defp has_logged_transaction_data(logger_data) do
#     String.downcase(logger_data["blockHash"] || "") ===
#       String.downcase("0xafa5f9c7332314ac7d72604e7f7dccd4a570ca8c65be2b09e4c4a22325e493f4") and
#       String.downcase(logger_data["hash"] || "") ===
#         String.downcase("0x4a4978a22dd2313fbfd438d417a8bed745a3fc1f2da65e630c2b54d15b5a5630") and
#       logger_data["blockNumber"] === 7_112_384 and
#       String.downcase(logger_data["contractAddress"]) ===
#         String.downcase("0x174bfa6600bf90c885c7c01c7031389ed1461ab9") and
#       logger_data["nonce"] === 206 and logger_data["txreceipt_status"] === "1" and
#       logger_data["transactionIndex"] === 61 and logger_data["value"] === 0

#     # There are more values, but these seem like enough to test.
#   end

#   # Checks that our event name and argument values exist in the logs.
#   defp has_logged_event_data(logger_data) do
#     logger_data["eventName"] === "Transfer" and
#       logger_data["eventArgumentValues"] === %{
#         "value" => 19_616_225_810_000_000_000,
#         "from" => "0x9997fce91663a1ce4a61e5d1bbbfeed0838dc5cf",
#         "to" => "0x1bddbe8381c3f6d2693bb411ef89e535ca1193a4"
#       }
#   end

#   # Some spot-checks to see that the log's generic data (gas price, etc) has been output.
#   defp has_logged_generic_log_stats(logger_data) do
#     logger_data["blockNumber"] === 7_854_907 and
#       String.downcase(logger_data["address"]) ===
#         String.downcase("0x174BfA6600Bf90C885c7c01C7031389ed1461Ab9") and
#       logger_data["gasPrice"] === 20_000_000_000 and logger_data["gasUsed"] === 38187 and
#       logger_data["topic"] === [
#         "0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef",
#         "0x0000000000000000000000009997fce91663a1ce4a61e5d1bbbfeed0838dc5cf",
#         "0x0000000000000000000000001bddbe8381c3f6d2693bb411ef89e535ca1193a4"
#       ]
#   end
# end
